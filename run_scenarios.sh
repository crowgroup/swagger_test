#!/bin/bash
echo Run scenarios is starting...
pwd
if [ ! -d ../outputs ]
then
    mkdir ../outputs
fi

python -m common.download_yaml
pytest --junitxml=results/result1.xml scenarios/test_authentication.py -v -s | tee outputs/autentication.out
pytest --junitxml=results/result2.xml scenarios/test_panels.py -v -s | tee outputs/panels.out
pytest --junitxml=results/result3.xml scenarios/test_areas.py -v -s | tee outputs/areas.out
pytest --junitxml=results/result4.xml scenarios/test_users.py -v -s | tee outputs/users.out
pytest --junitxml=results/result5.xml scenarios/test_zones.py -v -s | tee outputs/zones.out
pytest --junitxml=results/result6.xml scenarios/test_outputs.py -v -s | tee outputs/outputs.out
pytest --junitxml=results/result7.xml scenarios/test_troubles.py -v -s | tee outputs/troubles.out
pytest --junitxml=results/result8.xml scenarios/test_events_and_pictures.py -v -s | tee outputs/events_and_pict.out
pytest --junitxml=results/result9.xml scenarios/test_controls.py -v -s | tee outputs/controls.out
pytest --junitxml=results/result10.xml scenarios/test_connection.py -v -s | tee outputs/connections.out
pytest --junitxml=results/result11.xml scenarios/test_communication.py -v -s | tee outputs/communication.out
pytest --junitxml=results/result12.xml scenarios/test_measurements.py -v -s | tee outputs/measurements.out
pytest --junitxml=results/result13.xml scenarios/test_configuration.py -v -s | tee outputs/configuration.out
pytest --junitxml=results/result14.xml scenarios/test_smart_home.py -v -s | tee outputs/smart_home.out
pytest --junitxml=results/result15.xml scenarios/test_user_actions.py -v -s | tee outputs/user_actions.out
pytest --junitxml=results/result16.xml scenarios/test_dect.py -v -s | tee outputs/dect.out
