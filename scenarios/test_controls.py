"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import login
from sub_swagger_test.url_functions import panic_put


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=3)
def test_login():
    """
    Login
    """
    url_templ = '/panels/{control_panel_mac}/login/'
    path_value = CROW_DOC_PATHS[url_templ]

    # POST
    post_val = path_value.get('post', None)
    if post_val is not None:
        format_dic = {}
        header_dic = {}
        params = post_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        login(url, header_dic)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=4)
def test_put_panic():
    """
    Make a panic alarm
    """
    url_templ = '/panels/{control_panel_mac}/panic/'
    path_value = CROW_DOC_PATHS[url_templ]

    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        panic_put(url, header_dic)
