"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import users_get
from sub_swagger_test.url_functions import users_user_get
from sub_swagger_test.url_functions import users_user_put


@pytest.mark.run(order=1)
def test_get_users_list():
    """
    Get list of Users
    """
    url_templ = '/panels/{control_panel_mac}/users/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        # print url_templ + ' GET:', get_val
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        users_get(url, header_dic)


# @pytest.mark.skip(reason='not relevant')
# # @pytest.mark.run(order=4)
# def test_user_login():
#     login()


@pytest.mark.run(order=2)
def test_get_user_detail():
    """
    Get a specific User(by user_id)
    """
    url_templ = '/panels/{control_panel_mac}/users/{user_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'userIdParam' in list(param.values())[0]:
                format_dic.update({'user_id': DEFAULT_IDS['user_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        users_user_get(url, header_dic)


@pytest.mark.run(order=3)
def test_put_user_detail():
    """
    Update a specific User(by user_id)
    """
    url_templ = '/panels/{control_panel_mac}/users/{user_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # installer_mode('post') Igor TMP
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'userIdParam' in list(param.values())[0]:
                format_dic.update({'user_id': DEFAULT_IDS['user_id']+1})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        users_user_put(url, header_dic)

    # installer_mode('delete') Igor TMP

# @pytest.mark.run(order=11)
# def test_print_users():
#     print 'USERS:', USER_IDS
