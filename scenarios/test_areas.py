"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.scenarios.test_configuration import installer_mode
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import areas_area_get
from sub_swagger_test.url_functions import areas_area_patch
from sub_swagger_test.url_functions import areas_area_put
from sub_swagger_test.url_functions import areas_get

AREA_IDS = []


@pytest.mark.run(order=1)
def test_get_areas_list():
    """
    Get list of areas for a panel
    """
    url_templ = '/panels/{control_panel_mac}/areas/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        areas_get(url, header_dic, AREA_IDS)
        print('Areas GET:', AREA_IDS)


@pytest.mark.run(order=2)
def test_get_area_details():
    """
    Get a specific area(by area_id)
    """
    url_templ = '/panels/{control_panel_mac}/areas/{area_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'areaIdParam' in list(param.values())[0]:
                format_dic.update({'area_id': DEFAULT_IDS['area_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        areas_area_get(url, header_dic)


@pytest.mark.run(order=4)
def test_put_area():
    """
    Set an Area's configuration
    """
    url_templ = '/panels/{control_panel_mac}/areas/{area_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'areaIdParam' in list(param.values())[0]:
                format_dic.update({'area_id': DEFAULT_IDS['area_id']+1})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        areas_area_put(url, header_dic)

    installer_mode('delete')


def area_arm_prepare():
    """
    Prepare URL and headers for change the area state
    :return: URL, headers
    """
    url_templ = '/panels/{control_panel_mac}/areas/{area_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # PATCH
    patch_val = path_value.get('patch', None)
    if patch_val is not None:
        format_dic = {}
        header_dic = {}
        params = patch_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'areaIdParam' in list(param.values())[0]:
                format_dic.update({'area_id': DEFAULT_IDS['area_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        return url, header_dic


@pytest.mark.run(order=5)
def test_area_arm():
    """
    Change the area state (ARM)
    """
    url, header_dic = area_arm_prepare()
    areas_area_patch(url, header_dic, state='arm', force=True)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=6)
def test_area_disarm():
    """
    Change the area state (DISARM)
    """
    url, header_dic = area_arm_prepare()
    areas_area_patch(url, header_dic, state='disarm', force=False)


# @pytest.mark.run(order=7)
# def test_get_events():
#     events_get()
#
#
# @pytest.mark.run(order=8)
# def test_exit_installer_mode():
#     config_mode_delete()






