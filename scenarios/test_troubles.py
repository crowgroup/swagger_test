"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import troubles_delete
from sub_swagger_test.url_functions import troubles_get


@pytest.mark.run(order=1)
def test_get_troubles():
    """
    Get General Troubles for a panel
    """
    url_templ = '/panels/{control_panel_mac}/troubles/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        troubles_get(url, header_dic)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=2)
def test_delete_troubles():
    """
    Reset General Troubles for a panel
    """
    url_templ = '/panels/{control_panel_mac}/troubles/'
    path_value = CROW_DOC_PATHS[url_templ]

    # DELETE
    del_val = path_value.get('delete', None)
    if del_val is not None:
        format_dic = {}
        header_dic = {}
        params = del_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        troubles_delete(url, header_dic)


# # @pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=2)
# def test_get_troubles_after_del():
#     troubles_get()







