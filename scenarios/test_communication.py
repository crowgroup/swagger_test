"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.scenarios.test_configuration import installer_mode
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import ethernet_config_get
from sub_swagger_test.url_functions import ethernet_config_put
from sub_swagger_test.url_functions import ethernet_info_get
from sub_swagger_test.url_functions import gsm_config_get
from sub_swagger_test.url_functions import gsm_config_put
from sub_swagger_test.url_functions import gsm_info_get
from sub_swagger_test.url_functions import radio_info_get
from sub_swagger_test.url_functions import wifi_config_get
from sub_swagger_test.url_functions import wifi_config_put
from sub_swagger_test.url_functions import wifi_info_get


@pytest.mark.run(order=1)
def test_get_ethernet_info():
    """
    Get Ethernet module state
    """
    url_templ = '/panels/{control_panel_mac}/module/ethernet/info/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        ethernet_info_get(url, header_dic)


@pytest.mark.run(order=2)
def test_get_wifi_info():
    """
    Get WiFi module state
    """
    url_templ = '/panels/{control_panel_mac}/module/wifi/info/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        wifi_info_get(url, header_dic)


@pytest.mark.run(order=3)
def test_get_gsm_info():
    """
    Get GSM module state
    """
    url_templ = '/panels/{control_panel_mac}/module/gsm/info/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        gsm_info_get(url, header_dic)


@pytest.mark.run(order=4)
def test_get_radio_info():
    """
    Get Radio module state
    """
    url_templ = '/panels/{control_panel_mac}/module/radio/info/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        radio_info_get(url, header_dic)


@pytest.mark.run(order=5)
def test_get_ethernet_config():
    """
    Get Ethernet module configuration
    """
    url_templ = '/panels/{control_panel_mac}/module/ethernet/config/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        ethernet_config_get(url, header_dic)


@pytest.mark.run(order=6)
def test_get_wifi_config():
    """
    Get WiFi module configuration
    """
    url_templ = '/panels/{control_panel_mac}/module/wifi/config/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        wifi_config_get(url, header_dic)


@pytest.mark.run(order=7)
def test_get_gsm_config():
    """
    Get GSM module configuration
    """
    url_templ = '/panels/{control_panel_mac}/module/gsm/config/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        gsm_config_get(url, header_dic)


# @pytest.mark.skip(reason='trying')
@pytest.mark.run(order=8)
# @pytest.mark.xfail
def test_put_ethernet_config():
    """
    Set Ethernet module configuration
    """
    url_templ = '/panels/{control_panel_mac}/module/ethernet/config/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        ethernet_config_put(url, header_dic)

    installer_mode('delete')


# @pytest.mark.skip(reason='trying')
@pytest.mark.run(order=9)
def test_put_wifi_config():
    """
    Set WiFi module configuration
    """
    url_templ = '/panels/{control_panel_mac}/module/wifi/config/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        wifi_config_put(url, header_dic)

    installer_mode('delete')


# @pytest.mark.skip(reason='trying')
@pytest.mark.run(order=10)
def test_put_gsm_config():
    """
    Set GSM module configuration
    """
    url_templ = '/panels/{control_panel_mac}/module/gsm/config/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        gsm_config_put(url, header_dic)

    installer_mode('delete')
