"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import endpoint_get


@pytest.mark.run(order=1)
def test_get_users_list():
    """
    Get list of users
    """
    url = '/user_actions/endpoint/'
    path_value = CROW_DOC_PATHS[url]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        url = CONFIG_DATA['url_base'] + url
        endpoint_get(url)
