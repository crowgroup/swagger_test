"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import measurements_latest_get
from sub_swagger_test.url_functions import measurements_specific_device

MEASUR_DEVICE = {}


@pytest.mark.run(order=1)
def test_get_measurements_latest():
    """
    Get latest measurements
    """
    url_templ = '/panels/{control_panel_mac}/measurements/latest/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        # header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)

        res = measurements_latest_get(url)
        MEASUR_DEVICE.update(res)


@pytest.mark.run(order=1)
def test_get_measur_specif_device():
    """
    Get measurements for a specific device
    """
    if not len(MEASUR_DEVICE):
        print('\nNo measure device')
        return

    url_templ = '/panels/{control_panel_mac}/measurements/{device_type}/{device_id}/interface/{interface_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        # header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})

        format_dic.update(MEASUR_DEVICE)
        print('\nFormat dic:', format_dic)
        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        print('URL:', url)
        measurements_specific_device(url)
