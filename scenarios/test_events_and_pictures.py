"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import events_get
from sub_swagger_test.url_functions import pictures_get
from sub_swagger_test.url_functions import pictures_picture_delete
from sub_swagger_test.url_functions import pictures_picture_get
from sub_swagger_test.url_functions import zones_zone_picture_post
from sub_swagger_test.url_functions import zones_zone_pictures_get

PICTURE_IDS = []


@pytest.mark.run(order=1)
def test_get_events():
    """
    Retrieve a list of events
    """
    url_templ = '/panels/{control_panel_mac}/events/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})

        events_get(CONFIG_DATA['url_base'] + url_templ.format(**format_dic))


@pytest.mark.run(order=2)
def test_get_pictures():
    """
    List of pictures for panel
    """
    url_templ = '/panels/{control_panel_mac}/pictures/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})

        pictures_get(CONFIG_DATA['url_base'] + url_templ.format(**format_dic), PICTURE_IDS)
    print('Pictures:', PICTURE_IDS)


@pytest.mark.run(order=3)
def test_get_pictures_picture():
    """
    Retrieve a specific picture by id
    """
    url_templ = '/panels/{control_panel_mac}/pictures/{id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif param.get('required', None):
                format_dic.update({param['name']: PICTURE_IDS[-1]})

        pictures_picture_get(CONFIG_DATA['url_base'] + url_templ.format(**format_dic))


@pytest.mark.run(order=5)
def test_get_zone_pictures():
    """
    List of Zone's pictures
    """
    url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/pictures/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'zoneIdParam' in list(param.values())[0]:
                format_dic.update({'zone_id': DEFAULT_IDS['zone_id']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        zones_zone_pictures_get(url)


# @pytest.mark.skip(reason='not ready')
# @pytest.mark.xfail
@pytest.mark.run(order=6)
def test_post_zone_picture():
    """
    Take a picture in specific zone
    """
    url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/pictures/'
    path_value = CROW_DOC_PATHS[url_templ]

    # POST
    post_val = path_value.get('post', None)
    if post_val is not None:
        format_dic = {}
        header_dic = {}
        params = post_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'zoneIdParam' in list(param.values())[0]:
                format_dic.update({'zone_id': DEFAULT_IDS['zone_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        pic_id = zones_zone_picture_post(url, header_dic)
        PICTURE_IDS.append(pic_id)


@pytest.mark.run(order=7)
def test_delete_pictures_picture():
    """
    Delete a specific picture by id
    """
    url_templ = '/panels/{control_panel_mac}/pictures/{id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # DELETE
    del_val = path_value.get('get', None)
    if del_val is not None:
        format_dic = {}
        params = del_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif param.get('required', None):
                print('\nPicture id to del:', PICTURE_IDS[-1])
                format_dic.update({param['name']: PICTURE_IDS[-1]})

        pictures_picture_delete(CONFIG_DATA['url_base'] + url_templ.format(**format_dic))


