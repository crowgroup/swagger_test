"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.scenarios.test_configuration import installer_mode
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import zones_get
from sub_swagger_test.url_functions import zones_post
from sub_swagger_test.url_functions import zones_zone_delete
from sub_swagger_test.url_functions import zones_zone_get
from sub_swagger_test.url_functions import zones_zone_patch
from sub_swagger_test.url_functions import zones_zone_put

ZONE_IDS = []


@pytest.mark.run(order=1)
def test_get_zones_list():
    """
    Get list of Zones for a panel
    """
    url_templ = '/panels/{control_panel_mac}/zones/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        zones_get(url, header_dic, ZONE_IDS)
        print('Zones GET:', ZONE_IDS)


@pytest.mark.run(order=2)
def test_get_zone_detail():
    """
    Get a specific Zone(by zone_id)
    """
    url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'zoneIdParam' in list(param.values())[0]:
                format_dic.update({'zone_id': DEFAULT_IDS['zone_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        zones_zone_get(url, header_dic)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=3)
def test_add_zone():
    """
    Add a new Zone for a panel
    """
    url_templ = '/panels/{control_panel_mac}/zones/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # POST
    post_val = path_value.get('post', None)
    if post_val is not None:
        format_dic = {}
        header_dic = {}
        params = post_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        zone_id = zones_post(url, header_dic)
        ZONE_IDS.append(zone_id)
        ZONE_IDS.sort()

    installer_mode('delete')
    print('ZONES after add:', ZONE_IDS)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=4)
def test_put_zone():
    """
    Set a Zone's configuration
    """
    url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'zoneIdParam' in list(param.values())[0]:
                format_dic.update({'zone_id': DEFAULT_IDS['zone_id']+1})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        zones_zone_put(url, header_dic)

    installer_mode('delete')


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=5)
def test_delete_zone():
    """
    Delete a Zone
    """
    url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # DELETE
    zone_id = DEFAULT_IDS['zone_id'] + 1
    del_val = path_value.get('delete', None)
    if del_val is not None:
        format_dic = {}
        header_dic = {}
        params = del_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'zoneIdParam' in list(param.values())[0]:
                format_dic.update({'zone_id': zone_id})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        zones_zone_delete(url, header_dic)

    installer_mode('delete')
    ZONE_IDS.remove(zone_id)
    print('ZONES after delete:', ZONE_IDS)


# @pytest.mark.skip(reason='not relevant')
# # @pytest.mark.run(order=6)
# def test_get_zone_params():  # Missed in Swagger ?????
#     url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/params/'# Missed in Swagger ?????
#     path_value = CROW_DOC_PATHS[url_templ]
#
#     installer_mode('post')
#     # POST
#     post_val = path_value.get('post', None)
#     if post_val is not None:
#         format_dic = {}
#         header_dic = {}
#         params = post_val['parameters']
#         for param in params:
#             if 'panelMACParam' in param.values()[0]:
#                 format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
#             elif 'zoneIdParam' in param.values()[0]:
#                 format_dic.update({'zone_id': DEFAULT_IDS['zone_id']+1})
#             elif 'remotePasswordParam' in param.values()[0]:
#                 header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
#             elif 'userCodeParam' in param.values()[0]:
#                 header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})
#
#         url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
#         zones_zone_params_get(url, header_dic)
#
#     installer_mode('delete')
#     print 'ZONES after add:', ZONE_IDS


# @pytest.mark.skip(reason='not relevant')
# # @pytest.mark.run(order=7)
# def test_put_zone_params():  # Missed in Swagger ?????
#     zones_zone_params_put(ZONE_IDS[-1])


def prepare_zone_bypass():
    """
    Prepare bypass a Zone
    :return: URL, headers
    """
    url_templ = '/panels/{control_panel_mac}/zones/{zone_id}/'
    path_value = CROW_DOC_PATHS[url_templ]
    url = ''
    header_dic = {}

    # PATCH
    patch_val = path_value.get('patch', None)
    if patch_val is not None:
        format_dic = {}
        params = patch_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'zoneIdParam' in list(param.values())[0]:
                format_dic.update({'zone_id': DEFAULT_IDS['zone_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)

    return url, header_dic


@pytest.mark.run(order=11)
# @pytest.mark.skip(reason='fails on timeout')
def test_zone_bypass_do():
    """
    Bypass a Zone
    """
    url, header_dic = prepare_zone_bypass()
    zones_zone_patch(url, header_dic, bypass=True)


@pytest.mark.run(order=12)
# @pytest.mark.skip(reason='fails on timeout')
def test_zone_bypass_cancel():
    """
    Cancel bypass a Zone
    """
    url, header_dic = prepare_zone_bypass()
    zones_zone_patch(url, header_dic, bypass=False)


@pytest.mark.run(order=13)
def test_print_zones():
    """
    Print list of Zones
    """
    print('ZONES:', ZONE_IDS)
