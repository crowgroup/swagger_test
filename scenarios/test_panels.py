"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions.get_url_functions import panels_get
from sub_swagger_test.url_functions.get_url_functions import panels_panel_get
from sub_swagger_test.url_functions.put_url_functions import panels_panel_put

PANEL_IDS = []


@pytest.mark.run(order=1)
def test_get_panels_list():
    """
    Retrieve a list of panels for the current user
    """
    print('URL_BASE:', CONFIG_DATA['url_base'])
    url_templ = '/panels/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        panels_get(CONFIG_DATA['url_base'] + url_templ, PANEL_IDS)
        print('Panels ', PANEL_IDS)


@pytest.mark.run(order=2)
def test_get_panel():
    """
    Retrieve a panel association by id
    """
    url_templ = '/panels/{mac}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        params = get_val['parameters']
        for param in params:
            if param.get('required', None):
                format_dic.update({param['name']: DEFAULT_IDS[param['name']]})

        panels_panel_get(CONFIG_DATA['url_base'] + url_templ.format(**format_dic))


@pytest.mark.run(order=3)
def test_put_panel():
    """
    Update a specific panel association
    """
    url_templ = '/panels/{mac}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        params = put_val['parameters']
        for param in params:
            if param.get('required', None):
                format_dic.update({param['name']: DEFAULT_IDS[param['name']]})

        panels_panel_put(CONFIG_DATA['url_base'] + url_templ.format(**format_dic))


