"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.scenarios.test_configuration import installer_mode
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import dect_delete
from sub_swagger_test.url_functions import dect_get
from sub_swagger_test.url_functions import dect_list_get

DECT_IDS = []


# # @pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=3)
# def test_enter_installer_mode():
#     config_mode_post()


@pytest.mark.run(order=1)
def test_get_dect_list():
    """
    Get list of paired dect devices
    """
    url_templ = '/panels/{control_panel_mac}/dect/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        dect_list_get(url, header_dic, dect_ids=DECT_IDS)

    print('DECTs:', DECT_IDS)


# @pytest.mark.skip(reason='not relevant')
@pytest.mark.run(order=2)
def test_get_dect_detail():
    """
    Get a specific device by device_id
    """
    url_templ = '/panels/{control_panel_mac}/dect/{device_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'dectDeviceIdParam' in list(param.values())[0]:
                format_dic.update({'device_id': DEFAULT_IDS['device_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        dect_get(url, header_dic)


# @pytest.mark.skip(reason='not relevant')
# # @pytest.mark.run(order=7)
# def test_delete_dect_list():
#     dect_list_delete()


@pytest.mark.skip(reason='not relevant')
# @pytest.mark.run(order=6)
def test_delete_dect():
    """
    Delete/UnPair a DECT device
    """
    url_templ = '/panels/{control_panel_mac}/dect/{device_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # DELETE
    del_val = path_value.get('delete', None)
    if del_val is not None:
        format_dic = {}
        header_dic = {}
        params = del_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'dectDeviceIdParam' in list(param.values())[0]:
                format_dic.update({'device_id': DEFAULT_IDS['device_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        dect_delete(url, header_dic)

    installer_mode('delete')


# @pytest.mark.run(order=5)
# def test_print_dect():
#     print 'DECTs:', DECT_IDS
#
#
# @pytest.mark.skip(reason='not relevant')
# # @pytest.mark.run(order=4)
# def test_post_dect():
#     dect_post()
#
#
# @pytest.mark.run(order=8)
# def test_exit_installer_mode():
#     config_mode_delete()
#
#
# @pytest.mark.run(order=2)
# def test_get_dect_latest_measurements():
#     dect_measurements_latest_get()
#
#
# # @pytest.mark.run(order=9)
# # def test_print_dect2():
# #     print 'DECTs:', DECT_IDS
