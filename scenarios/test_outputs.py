"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.scenarios.test_configuration import installer_mode
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import outputs_get
from sub_swagger_test.url_functions import outputs_output_delete
from sub_swagger_test.url_functions import outputs_output_get
from sub_swagger_test.url_functions import outputs_output_patch
from sub_swagger_test.url_functions import outputs_output_put
from sub_swagger_test.url_functions import outputs_post

OUTPUT_IDS = []


@pytest.mark.run(order=1)
def test_get_outputs_list():
    """
    Get list of Outputs for a panel
    """
    url_templ = '/panels/{control_panel_mac}/outputs/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        outputs_get(url, header_dic, OUTPUT_IDS)
        print('Outputs GET:', OUTPUT_IDS)


@pytest.mark.run(order=2)
def test_get_output_detail():
    """
    Get a specific Output(by output_id) for a panel
    """
    url_templ = '/panels/{control_panel_mac}/outputs/{output_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    # GET
    get_val = path_value.get('get', None)
    if get_val is not None:
        format_dic = {}
        header_dic = {}
        params = get_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'outputIdParam' in list(param.values())[0]:
                format_dic.update({'output_id': DEFAULT_IDS['output_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        outputs_output_get(url, header_dic)


@pytest.mark.run(order=3)
def test_add_output():
    """
    Add a new Output for a panel
    """
    url_templ = '/panels/{control_panel_mac}/outputs/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # POST
    post_val = path_value.get('post', None)
    if post_val is not None:
        format_dic = {}
        header_dic = {}
        params = post_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        output_id = outputs_post(url, header_dic)
        OUTPUT_IDS.append(output_id)
        OUTPUT_IDS.sort()

    installer_mode('delete')
    print('OUTPUTS after add:', OUTPUT_IDS)


@pytest.mark.run(order=4)
def test_put_output():
    """
    Set an Output's configuration
    """
    url_templ = '/panels/{control_panel_mac}/outputs/{output_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # PUT
    put_val = path_value.get('put', None)
    if put_val is not None:
        format_dic = {}
        header_dic = {}
        params = put_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'outputIdParam' in list(param.values())[0]:
                format_dic.update({'output_id': DEFAULT_IDS['output_id']+1})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        outputs_output_put(url, header_dic)

    installer_mode('delete')


# @pytest.mark.skip(reason='not relevant')
# # @pytest.mark.xfail
@pytest.mark.run(order=5)
def test_delete_output():
    """
    Delete an Output
    """
    url_templ = '/panels/{control_panel_mac}/outputs/{output_id}/'
    path_value = CROW_DOC_PATHS[url_templ]

    installer_mode('post')
    # DELETE
    output_id = DEFAULT_IDS['output_id'] + 1
    del_val = path_value.get('delete', None)
    if del_val is not None:
        format_dic = {}
        header_dic = {}
        params = del_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'outputIdParam' in list(param.values())[0]:
                format_dic.update({'output_id': output_id})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
        outputs_output_delete(url, header_dic)

    installer_mode('delete')
    OUTPUT_IDS.remove(output_id)
    print('OUTPUTS after delete:', OUTPUT_IDS)


def prepare_output_status_change():
    """
    Prepare turn output
    :return: URL, headers
    """
    url_templ = '/panels/{control_panel_mac}/outputs/{output_id}/'
    path_value = CROW_DOC_PATHS[url_templ]
    url = ''
    header_dic = {}

    # PATCH
    patch_val = path_value.get('patch', None)
    if patch_val is not None:
        format_dic = {}
        params = patch_val['parameters']
        for param in params:
            if 'panelMACParam' in list(param.values())[0]:
                format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
            elif 'outputIdParam' in list(param.values())[0]:
                format_dic.update({'output_id': DEFAULT_IDS['output_id']})
            elif 'remotePasswordParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
            elif 'userCodeParam' in list(param.values())[0]:
                header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

        url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)

    return url, header_dic


@pytest.mark.run(order=11)
@pytest.mark.xfail
# @pytest.mark.skip(reason='fails on timeout')
def test_output_turn_on():
    """
    Turn output ON
    """
    url, header_dic = prepare_output_status_change()
    outputs_output_patch(url, header_dic, do_state=True)


@pytest.mark.run(order=12)
@pytest.mark.xfail
# @pytest.mark.skip(reason='fails on timeout')
def test_output_turn_off():
    """
    Turn output OFF
    """
    url, header_dic = prepare_output_status_change()
    outputs_output_patch(url, header_dic, do_state=False)


@pytest.mark.run(order=18)
def test_print_outputs():
    """
    Print list of Outputs
    """
    print('OUTPUTS:', OUTPUT_IDS)
