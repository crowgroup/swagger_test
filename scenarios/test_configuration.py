"""
IgorG
"""
import pytest

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import DEFAULT_IDS
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.url_functions import config_mode_delete
from sub_swagger_test.url_functions import config_mode_get
from sub_swagger_test.url_functions import config_mode_post


def installer_mode(mode):
    """
    Request installer mode
    :param mode: 'get', 'post', 'delete'
    """
    url_templ = '/panels/{control_panel_mac}/config_mode/'
    path_value = CROW_DOC_PATHS[url_templ]

    if mode == 'get':
        get_val = path_value.get('get', None)
        if get_val is not None:
            format_dic = {}
            header_dic = {}
            params = get_val['parameters']
            for param in params:
                if 'panelMACParam' in list(param.values())[0]:
                    format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
                elif 'remotePasswordParam' in list(param.values())[0]:
                    header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
                # elif 'userCodeParam' in param.values()[0]:
                #     header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

            url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
            mode = config_mode_get(url, header_dic)
            if mode == 0:
                print('Not in config mode')
            else:
                print('In config mode')

    elif mode == 'post':
        post_val = path_value.get('post', None)
        if post_val is not None:
            format_dic = {}
            header_dic = {}
            params = post_val['parameters']
            for param in params:
                if 'panelMACParam' in list(param.values())[0]:
                    format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
                elif 'remotePasswordParam' in list(param.values())[0]:
                    header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
                # elif 'userCodeParam' in param.values()[0]:
                    # header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})
                elif 'installerCodeParam' in list(param.values())[0]:
                    header_dic.update({'X-Crow-CP-Installer': CONFIG_DATA['X-Crow-CP-Installer']})

            # header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})
            url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
            config_mode_post(url, header_dic)

    elif mode == 'delete':
        del_val = path_value.get('delete', None)
        if del_val is not None:
            format_dic = {}
            header_dic = {}
            params = del_val['parameters']
            for param in params:
                if 'panelMACParam' in list(param.values())[0]:
                    format_dic.update({'control_panel_mac': DEFAULT_IDS['control_panel_mac']})
                elif 'remotePasswordParam' in list(param.values())[0]:
                    header_dic.update({'X-Crow-CP-Remote': CONFIG_DATA['X-Crow-CP-Remote']})
                # elif 'userCodeParam' in param.values()[0]:
                #     header_dic.update({'X-Crow-CP-User': CONFIG_DATA['X-Crow-CP-User']})

            url = CONFIG_DATA['url_base'] + url_templ.format(**format_dic)
            config_mode_delete(url, header_dic)

    else:
        msg = 'mode {} does not supported'
        raise ValueError(msg)


@pytest.mark.run(order=1)
def test_installer_mode():
    """
    Test all installer modes
    """
    installer_mode('get')
    # time.sleep(1)
    installer_mode('post')
    installer_mode('delete')


@pytest.mark.run(order=2)
def test_get_config_backup():
    """
    Get config backup
    """
    pass


