#!/bin/bash
#pwd
if [ -d $WORKSPACE ]
then
    PYENV_HOME=$WORKSPACE/.pyenv

    # Delete previously built virtualenv
    if [ -d $PYENV_HOME ]
    then
        rm -rf $PYENV_HOME
    fi

    # Create virtualenv and install necessary packages
    virtualenv --no-site-packages $PYENV_HOME
    source $PYENV_HOME/bin/activate
    pip install -r requirements.txt

    # Copy .pylintrc to jenkins' root
    cp pylints/.pylintrc ~jenkins/
fi
pylint *.py | tee pylints/pylint.out
pylint **/*.py | tee -a pylints/pylint.out
#find . -name "*.py" | xargs pylint | tee pylints/pylint.out