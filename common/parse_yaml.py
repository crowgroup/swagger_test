"""
IgorG
"""
from collections import OrderedDict
import yaml
from sub_swagger_test.common.common_stuff import CONFIG_DATA


def ordered_load_crowdoc(crow_doc_path, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    """
    Load 'crowdoc.yaml' to OrderedDict
    :param crow_doc_path: str
    :param Loader: yaml.Loader
    :param object_pairs_hook: OrderedDict
    :return: OrderedDict obj
    """

    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        """
        Construct mapping
        :param loader: Loader
        :param node: Node
        :return: OrderedDict
        """
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))

    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)

    with open(crow_doc_path) as yaml_stream:
        dic = yaml.load(yaml_stream, OrderedLoader)

    return dic


# download_yaml_file()
CROW_DOC_PATHS = ordered_load_crowdoc(CONFIG_DATA['path_yaml'])['paths']
# print('LEN keys:', len(list(CROW_DOC_PATHS.keys())))
