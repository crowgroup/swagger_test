"""
IgorG
"""
import base64
import requests
from sub_swagger_test.common.parse_yaml import CROW_DOC_PATHS
from sub_swagger_test.common.common_stuff import CONFIG_DATA


def login_get_token():
    """
    Get login token
    :return: str
    """
    url_templ = '/o/token/'
    path_value = CROW_DOC_PATHS[url_templ]
    post_val = path_value.get('post', None)
    if post_val is not None:
        client_id = client_secret = ''
        data_dic = {}
        params = post_val['parameters']
        # print 'PARAMS:', params
        for param in params:
            if param['required']:
                if param['name'] == 'username':
                    data_dic.update({'username': CONFIG_DATA['login']['username']})
                elif param['name'] == 'password':
                    data_dic.update({'password': CONFIG_DATA['login']['password']})
                elif param['name'] == 'grant_type':
                    data_dic.update({'grant_type': CONFIG_DATA['login']['grant_type']})
                elif param['name'] == 'client_id':
                    client_id = CONFIG_DATA['authorization']['client_id']
                elif param['name'] == 'client_secret':
                    client_secret = CONFIG_DATA['authorization']['client_secret']

        encoded = base64.urlsafe_b64encode((client_id + ':' + client_secret).encode())
        header_dic = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + encoded.decode()
        }

        url = CONFIG_DATA['url_base'] + url_templ
        resp = requests.post(url, data=data_dic, headers=header_dic)
        result = resp.json()
        print('\nAuthorization result: ', result)
        return result['access_token']


TOKEN = login_get_token()
