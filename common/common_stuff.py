"""
IgorG
"""
import os
import json
from requests.exceptions import HTTPError

# Zone Device types
SDEVTYPE_PIR = 0x31  # Infrared motion detector
SDEVTYPE_MAG = 0x32  # Magnetic contact
SDEVTYPE_RMT = 0x33  # Pendant remote control
SDEVTYPE_SMK = 0x34  # Smoke detector
SDEVTYPE_GAS = 0x35  # Gas detector
SDEVTYPE_GBD = 0x36  # Glass break detector
SDEVTYPE_CAM = 0x37  # Camera PIR
SDEVTYPE_FLD = 0x38  # Flood detector
SDEVTYPE_VIB = 0x39  # Vibration sensor
SDEVTYPE_HAM = 0x3A  # Home automation
SDEVTYPE_SPO = 0x3B  # Heart & saturation monitor
SDEVTYPE_TFM = 0x3C  # Temperature-flood-magnet detector
SDEVTYPE_SRN = 0x45  # Siren
SDEVTYPE_OUT = 0x57  # Output (relay board)
SDEVTYPE_KPD = 0x98  # Keypad
SDEVTYPE_SOC = 0xe7  # AC Socket

ZONE = 1
OUTPUT = 2

DEVICE_TYPES = {
    SDEVTYPE_PIR: ZONE,
    SDEVTYPE_MAG: ZONE,
    SDEVTYPE_RMT: ZONE,
    SDEVTYPE_SMK: ZONE,
    SDEVTYPE_GAS: ZONE,
    SDEVTYPE_GBD: ZONE,
    SDEVTYPE_CAM: ZONE,
    SDEVTYPE_FLD: ZONE,
    SDEVTYPE_VIB: ZONE,
    SDEVTYPE_HAM: OUTPUT,
    SDEVTYPE_SPO: ZONE,
    SDEVTYPE_TFM: ZONE,
    SDEVTYPE_SRN: OUTPUT,
    SDEVTYPE_OUT: OUTPUT,
    SDEVTYPE_KPD: ZONE,
    SDEVTYPE_SOC: OUTPUT,
}


def load_config_data():
    """
    Load data from 'config.json'
    :return: dict
    """
    config_file = os.path.join(os.path.dirname(__file__), 'config.json')
    with open(config_file) as flp:
        data = json.load(flp)

    # Define URL base path
    url_base = os.environ.get('URL_BASE')
    if url_base is not None:
        print('VAR URL_BASE:', url_base)
        data['url_base'] = url_base

    # Define local repo router and crowdoc.yaml paths
    jenkins_workspace = os.environ.get('WORKSPACE')
    if jenkins_workspace is not None:
        repo_router = '/var/lib/jenkins/workspace/router'
        path_yaml = os.path.join(jenkins_workspace, 'crowdoc.yaml')
        data['git_repo_local'] = repo_router
        data['path_yaml'] = path_yaml

    return data


CONFIG_DATA = load_config_data()

DEFAULT_IDS = {
    'id': CONFIG_DATA['panel_id'],  # TODO: in Swagger ask change to 'control_panel_id' , most likely to be del
    'control_panel_id': CONFIG_DATA['panel_id'],  # most likely to be del
    'mac': CONFIG_DATA['panel_mac'],
    'control_panel_mac': CONFIG_DATA['panel_mac'],
    'zone_id': 0,
    'output_id': 0,
    'user_id': 0,
    'picture_id': 0,  # No need
    'notification_id': 0,  # Not exists yet
    'device_type': 0,  # No need
    'device_id': 0,  # DECT usage
    'interface_id': 0,  # No need
    'wait_for': 0,  # ???
    'area_id': 0
}


def common_check(resp):
    """
    Check the response status
    :param resp: requests.Response
    :return: bool
    """
    status_code = resp.status_code
    print('status code:', status_code)
    try:
        resp.raise_for_status()
    except HTTPError as err:
        print(err)
        assert False

    assert status_code == 200

    if 'json' not in resp.headers['content-type']:
        print('Non-json response')
        assert False

    return True

