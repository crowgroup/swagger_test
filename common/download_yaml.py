import os
import shutil
import subprocess
import requests

from common.common_stuff import CONFIG_DATA


def download_yaml_file():
    """
    Download 'crowdoc.yaml'
    """
    # cwd = os.getcwd()
    # print('cwd:', cwd)
    # git_loc = CONFIG_DATA['git_repo_local']
    # print('git repo local:', git_loc)
    # if os.path.isdir(git_loc):
    #     os.chdir(git_loc)
    #     print('git pull to ', git_loc)
    #     subprocess.call(['git', 'pull'])
    # else:
    #     parent_git = os.path.dirname(git_loc)
    #     os.chdir(parent_git)
    #     print('git clone to ', parent_git)
    #     subprocess.call(['git', 'clone', CONFIG_DATA['git_repo_remote']])

    jenkins_workspace = os.environ.get('WORKSPACE')
    print('\nJenkins WORKSPACE', jenkins_workspace)

    path_yaml = os.path.abspath(CONFIG_DATA['path_yaml'])
    print('yaml path:', path_yaml)
    # shutil.copy(git_loc + '/api_doc/crowdoc.yaml', path_yaml)
    # os.chdir(cwd)

    resp = requests.get('https://bitbucket.org/crowgroup/router/raw/master/api_doc/crowdoc.yaml', verify=True,
                        auth=('igorg1', 'lant75ra'))
    data = resp.content.decode()
    print('data type:', type(data), 'len:', len(data))
    with open(path_yaml, 'w') as fp:
        fp.write(data)


download_yaml_file()
