from .get_url_functions import config_mode_get
from .get_url_functions import panels_get
from .get_url_functions import panels_panel_get
from .get_url_functions import pictures_get
from .get_url_functions import pictures_picture_get
from .get_url_functions import areas_get
from .get_url_functions import areas_area_get
from .get_url_functions import events_get
from .get_url_functions import zones_get
from .get_url_functions import zones_zone_get
# from get_url_functions import zones_zone_params_get
from .get_url_functions import zones_zone_pictures_get
from .get_url_functions import outputs_get
from .get_url_functions import outputs_output_get
from .get_url_functions import troubles_get
from .get_url_functions import users_get
from .get_url_functions import users_user_get
from .get_url_functions import connection_get
from .get_url_functions import dect_list_get
from .get_url_functions import dect_get
# from get_url_functions import dect_measurements_latest_get
from .get_url_functions import ethernet_info_get
from .get_url_functions import wifi_info_get
from .get_url_functions import gsm_info_get
from .get_url_functions import radio_info_get
from .get_url_functions import ethernet_config_get
from .get_url_functions import wifi_config_get
from .get_url_functions import gsm_config_get
from .get_url_functions import measurements_latest_get
from .get_url_functions import measurements_specific_device
from .get_url_functions import output_zones_get
from .get_url_functions import zone_outputs_get
from .get_url_functions import endpoint_get

from .post_url_functions import config_mode_post
from .post_url_functions import zones_post
from .post_url_functions import zones_zone_picture_post
from .post_url_functions import outputs_post
from .post_url_functions import login
# from post_url_functions import dect_post

from .put_url_functions import panels_panel_put
from .put_url_functions import areas_area_put
from .put_url_functions import zones_zone_put
# from put_url_functions import zones_zone_params_put
from .put_url_functions import outputs_output_put
from .put_url_functions import users_user_put
from .put_url_functions import connection_put
from .put_url_functions import panic_put
from .put_url_functions import ethernet_config_put
from .put_url_functions import wifi_config_put
from .put_url_functions import gsm_config_put

from .patch_url_functions import areas_area_patch
from .patch_url_functions import zones_zone_patch
from .patch_url_functions import outputs_output_patch

from .delete_url_functions import config_mode_delete
from .delete_url_functions import zones_zone_delete
from .delete_url_functions import outputs_output_delete
from .delete_url_functions import troubles_delete
from .delete_url_functions import pictures_picture_delete
from .delete_url_functions import dect_list_delete
from .delete_url_functions import dect_delete
from .delete_url_functions import connection_delete

