"""
IgorG
"""
import sys

import requests

from sub_swagger_test.common.common_stuff import common_check
from sub_swagger_test.common.login import TOKEN


def config_mode_get(url, header_dic):
    """
    Get the current configuration mode state
    :param url: URL
    :param header_dic: headers
    :return: 0 - not in config mode, 1 - in config mode
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('config: ', j_con['config'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True
    return j_con['config']


##############################
# #         Panels         # #
##############################

def panels_get(url, panel_ids):
    """
    Retrieve a list of panels
    :param url: URL
    :param panel_ids: list panel ids
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('Panels count:', j_cont['count'])
    results = j_cont['results']
    try:
        for result in results:
            print('name:', result['name'])
            panel_id = result['id']
            panel_ids.append(panel_id)
            print('id:', panel_id)
            print('mac:', result['mac'])
            print('version:', result['version'])
            print('state', result['state'], '\n')
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def panels_panel_get(url):
    """
    Retrieve a panel association by id
    :param url: URL
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('name:', j_cont['name'])
        print('id:', j_cont['id'])
        print('mac:', j_cont['mac'])
        print('version:', j_cont['version'])
        print('state', j_cont['state'], '\n')
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #           Areas        # #
##############################

def areas_get(url, header_dic, area_ids):
    """
    Get list of areas for a panel
    :param url: URL
    :param header_dic: headers
    :param area_ids: list areas ids
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        for area in j_cont:
            print('name:', area['name'])
            print('id:', area['id'])
            area_ids.append(area['id'])
            print('exit_delay:', area['exit_delay'])
            print('stay_exit_delay:', area['stay_exit_delay'])
            print('state:', area['state'])
            print('ready_to_arm:', area['ready_to_arm'])
            print('ready_to_stay:', area['ready_to_stay'])
            print('zone_alarm:', area['zone_alarm'])
            print('zone24H alarm:', area['zone24H_alarm'])

            # assert area['state'] == 'disarmed'
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def areas_area_get(url, header_dic):
    """
    Get a specific area
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('name:', j_cont['name'])
        print('id:', j_cont['id'])
        print('exit_delay:', j_cont['exit_delay'])
        print('stay_exit_delay:', j_cont['stay_exit_delay'])
        print('state:', j_cont['state'])
        print('ready_to_arm:', j_cont['ready_to_arm'])
        print('ready_to_stay:', j_cont['ready_to_stay'])
        print('zone_alarm:', j_cont['zone_alarm'])
        print('zone24H alarm:', j_cont['zone24H_alarm'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #           Zones        # #
##############################

def zones_get(url, header_dic, zone_ids):
    """
    Get list of Zones for a panel
    :param url: URL
    :param header_dic: headers
    :param zone_ids: list
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('zones {}'.format(len(j_cont)))
        for zone in j_cont:
            print('id:', zone['id'])
            zone_ids.append(zone['id'])
            print('name:', zone['name'])
            print('areas:', zone['areas'])
            print('flags:', zone.get('flags', None))
            print('hardware_version:', zone.get('hardware_version', None))
            print('software_version:', zone.get('software_version', None))
            print('rssi:', zone.get('rssi', 0))
            print('type:', zone['type'])
            print('device id:', zone.get('device_id', None))
            print('dect_id:{ipud:', zone['dect_id'].get('ipud', ''), 'unit_id:', zone['dect_id'].get('unit_id', 0), '}')
            print('work_mode id:', zone.get('work_mode', None))
            print('stay_mode_zone:', zone.get('stay_mode_zone', None))

    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def zones_zone_get(url, header_dic):
    """
    Get a specific Zone
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('flags:', j_cont.get('flags', None))
        print('name:', j_cont['name'])
        print('areas:', j_cont['areas'])
        print('hardware_version:', j_cont.get('hardware_version', None))
        print('software_version:', j_cont.get('software_version', None))
        print('rssi:', j_cont.get('rssi', 0))
        print('type:', j_cont['type'])
        print('id:', j_cont['id'])
        print('device id:', j_cont.get('device_id', None))
        print('dect_id:{ipud:', j_cont['dect_id'].get('ipud', ''), 'unit_id:', j_cont['dect_id'].get('unit_id', 0), '}')
        print('work_mode id:', j_cont.get('work_mode', None))
        print('stay_mode_zone:', j_cont.get('stay_mode_zone', None))
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


# def zones_zone_params_get(zone_id):  # missing in Swagger ????
#     print '\n[' + sys._getframe().f_code.co_name + ' on panel {}] id:{}'.format(PANEL_ID, zone_id)
#     url = URL_BASE + '/panels/{}/zones/{}/params/'.format(PANEL_ID, zone_id)
#     resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
#     res = common_check(resp)
#     assert res is True
#
#     j_cont = resp.json()
#     for key, val in j_cont.iteritems():
#         print '{}: {}'.format(key, val)
#
#     return j_cont


##############################
# #         Outputs        # #
##############################

def outputs_get(url, header_dic, output_ids):
    """
    Get list of Outputs for a panel
    :param url: URL
    :param header_dic: headers
    :param output_ids: list
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('outputs {}'.format(len(j_cont)))
        for output in j_cont:
            print('id:', output['id'])
            output_ids.append(output['id'])
            print('silent:', output.get('silent', 0))
            print('rssi:', output.get('rssi', 0))
            print('name:', output['name'])
            print('chime_mode:', output.get('chime_mode', 0))
            print('chime_timer:', output.get('chime_timer', 0))
            print('device id:', output.get('device_id', None))
            print('type:', output.get('type', 1))
            print('dect_id:' \
                  '{ipud:', output['dect_id'].get('ipud', ''), 'unit_id:', output['dect_id'].get('unit_id', 0), '}')
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def outputs_output_get(url, header_dic):
    """
    Get a specific Output(by output_id) for a panel
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('id:', j_cont['id'])
        print('silent:', j_cont.get('silent', 0))
        print('rssi:', j_cont.get('rssi', 0))
        print('name:', j_cont['name'])
        print('chime_mode:', j_cont.get('chime_mode', 0))
        print('chime_timer:', j_cont.get('chime_timer', 0))
        print('device id:', j_cont.get('device_id', None))
        print('type:', j_cont.get('type', 1))
        print('dect_id:{ipud:', j_cont['dect_id'].get('ipud', ''), 'unit_id:', j_cont['dect_id'].get('unit_id', 0), '}')
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #        Troubles        # #
##############################

def troubles_get(url, header_dic):
    """
    Get General Troubles for a panel
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    for key, val in j_cont.items():
        print('{}:'.format(key), val)
    # try:
    #     print 'can_bus_fail:', j_cont['can_bus_fail']
    #     print 'chime_is_off:', j_cont['chime_is_off']
    #     print 'code_attempts_alarm:', j_cont['code_attempts_alarm']
    #     print 'duress_alarm:', j_cont['duress_alarm']
    #     print 'ethernet_fail:', j_cont['ethernet_fail']
    #     print 'fire_alarm:', j_cont['fire_alarm']
    #     print 'fuse_fail:', j_cont['fuse_fail']
    #     print 'panel_tamper:', j_cont['panel_tamper']
    #     print 'gsm_line_fail:', j_cont['gsm_line_fail']
    #     print 'low_battery:', j_cont['low_battery']
    #     print 'mains_fail:', j_cont['mains_fail']
    #     print 'medical_alarm:', j_cont['medical_alarm']
    #     print 'panic_alarm:', j_cont['panic_alarm']
    #     print 'phone_line_fail:', j_cont['phone_line_fail']
    #     print 'real_time_clock_status:', j_cont['real_time_clock_status']
    #     print 'rf_jamming_alarm:', j_cont['rf_jamming_alarm']
    #     print 'gprs_fail:', j_cont['gprs_fail']
    #     print 'sdcard_fail:', j_cont['sdcard_fail']
    #     print 'wifi_fail:', j_cont['wifi_fail']
    #     print 'dect_fail:', j_cont['dect_fail']
    # except KeyError, e:
    #     print err
    #     res = False
    # finally:
    #     assert res is True


##############################
# #         Users          # #
##############################

def users_get(url, header_dic):
    """
    Get list of Users
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('users number: {}'.format(len(j_cont)))
        for idx, user in enumerate(j_cont):
            if idx >= 3:
                break
            print('id:', user['id'])
            # user_ids.append(user['id'])
            print('name:', user['name'])
            print('pendant_id:', user['pendant_id'])
            print('arm:', user['arm'])
            print('stay:', user['stay'])
            print('disarm:', user['disarm'])
            print('disarm_stay:', user['disarm_stay'])
            print('pendant_allow:', user['pendant_allow'])
            print('remote:', user['remote'])
            print('areas:', user['areas'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def users_user_get(url, header_dic):
    """
    Get a specific User(by user_id)
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('id:', j_cont['id'])
        print('name:', j_cont['name'])
        print('pendant_id:', j_cont['pendant_id'])
        print('arm:', j_cont['arm'])
        print('stay:', j_cont['stay'])
        print('disarm:', j_cont['disarm'])
        print('disarm_stay:', j_cont['disarm_stay'])
        print('pendant_allow:', j_cont['pendant_allow'])
        print('remote:', j_cont['remote'])
        print('areas:', j_cont['areas'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #         Events         # #
##############################

def events_get(url):
    """
    Retrieve a list of events
    :param url: URL
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        results = j_cont['results']
        for idx, result in enumerate(results):
            if idx >= 3:
                break
            print('control panel:', result['control_panel'])
            print('text:', result['text'])
            print('panel time:', result['panel_time'])
            print('zone:', result['zone'])
            pictures = result['pictures']
            for picture in pictures:
                print('picture url:', picture['url'])
            print('\n')
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #        Pictures        # #
##############################

def pictures_get(url, picture_ids):
    """
    List of pictures for panel
    :param url:URL
    :param picture_ids: list
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('count:', j_cont['count'])
    print('next:', j_cont['next'])
    print('previous:', j_cont['previous'])

    for idx, result in enumerate(j_cont['results']):
        if idx >= 3:
            break
        print('control_panel:', result['control_panel'])
        print('id:', result['id'])
        picture_ids.append(result['id'])
        print('created:', result['created'])
        print('panel_time:', result['panel_time'])
        print('url:', result['url'])
        print('zone:', result['zone'])
        print('zone_name:', result['zone_name'])
        print('event:', result['event'])
        print('key:', result['key'])
        print('type:', result['type'])
        print('pic_index:', result['pic_index'])
        print('pic_total:', result['pic_total'])


def pictures_picture_get(url):
    """
    Retrieve a specific picture by id
    :param url: URL
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('control_panel:', j_cont['control_panel'])
    print('id:', j_cont['id'])
    print('created:', j_cont['created'])
    print('panel_time:', j_cont['panel_time'])
    print('url:', j_cont['url'])
    print('zone:', j_cont['zone'])
    print('zone_name:', j_cont['zone_name'])
    print('event:', j_cont['event'])
    print('key:', j_cont['key'])
    print('type:', j_cont['type'])
    print('pic_index:', j_cont['pic_index'])
    print('pic_total:', j_cont['pic_total'])


def zones_zone_pictures_get(url):
    """
    List of Zone's pictures
    :param url: URL
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('count:', j_cont['count'])
    print('next:', j_cont['next'])
    print('previous:', j_cont['previous'])

    for idx, result in enumerate(j_cont['results']):
        if idx >= 3:
            break
        print('control_panel:', result['control_panel'])
        # assert PANEL_ID == result['control_panel']
        print('id:', result['id'])
        print('created:', result['created'])
        print('panel_time:', result['panel_time'])
        print('url:', result['url'])
        print('zone:', result['zone'])
        print('zone_name:', result['zone_name'])
        print('event:', result['event'])
        print('key:', result['key'])
        print('type:', result['type'])
        print('pic_index:', result['pic_index'])
        print('pic_total:', result['pic_total'])


##############################
# #      Connection        # #
##############################

def connection_get(url, header_dic):
    """
    Get the current connection state and interface
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('interface: ', j_con['interface'])
        print('connected: ', j_con['connected'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #    Communication       # #
##############################

def ethernet_info_get(url, header_dic):
    """
    Get Ethernet module state
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('ip: ', j_con['ip'])
        print('mac: ', j_con['mac'])
        print('mask: ', j_con['mask'])
        print('gateway: ', j_con['gateway'])
        print('id: ', j_con['id'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def wifi_info_get(url, header_dic):
    """
    Get WiFi module state
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('ssid: ', j_con['ssid'])
        print('ip: ', j_con['ip'])
        print('mask: ', j_con['mask'])
        print('gateway: ', j_con['gateway'])
        print('mac: ', j_con['mac'])
        print('dns: ', j_con['dns'])
        print('rssi: ', j_con['rssi'])
        print('id: ', j_con['id'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def gsm_info_get(url, header_dic):
    """
    Get GSM module state
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('status: ', j_con['status'])
        print('ip: ', j_con['ip'])
        print('provider: ', j_con['provider'])
        print('mask: ', j_con['mask'])
        print('status_desc: ', j_con['status_desc'])
        print('gateway: ', j_con['gateway'])
        print('band: ', j_con['band'])
        print('dns: ', j_con['dns'])
        print('module_hw: ', j_con['module_hw'])
        print('imei: ', j_con['imei'])
        print('rssi: ', j_con['rssi'])
        print('net: ', j_con['net'])
        print('id: ', j_con['id'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def radio_info_get(url, header_dic):
    """
    Get Radio module state
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('hardware_version: ', j_con['hardware_version'])
        print('software_version: ', j_con['software_version'])
        print('freq: ', j_con['freq'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def ethernet_config_get(url, header_dic):
    """
    Get Ethernet module configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('dns_server: ', j_con['dns_server'])
        print('static_ip: ', j_con['static_ip'])
        print('subnet_mask: ', j_con['subnet_mask'])
        print('gateway: ', j_con['gateway'])
        print('dhcp: ', j_con['dhcp'])
        print('enabled: ', j_con['enabled'])
        print('remote_control_port: ', j_con['remote_control_port'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def wifi_config_get(url, header_dic):
    """
    Get WiFi module configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('enabled: ', j_con['enabled'])
        print('id: ', j_con['id'])
        for conn in j_con['conn_config']:
            print('security: ', conn['security'])
            print('id: ', conn['id'])
            print('ssid: ', conn['ssid'])
            print('password: ', conn['password'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def gsm_config_get(url, header_dic):
    """
    Get GSM module configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('ussd: ', j_con['ussd'])
        print('apn: ', j_con['apn'])
        print('user: ', j_con['user'])
        print('password: ', j_con['password'])
        print('pin_code: ', j_con['pin_code'])
        print('sms: ', j_con['sms'])
        print('enabled: ', j_con['enabled'])
        print('id: ', j_con['id'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #     Measurements       # #
##############################

def measurements_latest_get(url):
    """
    Get latest measurments(
    :param url: URL
    :return: dict
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    print(j_con)
    out_dic = {}
    for meas in j_con:
        try:
            print('gas_value: ', meas['gas_value'])
            print('gas_level: ', meas['gas_level'])
            print('air_pressure: ', meas['air_pressure'])
            print('humidity: ', meas['humidity'])
            print('control_panel: ', meas['_id']['control_panel'])
            print('dect_interface: ', meas['_id']['dect_interface'])
            print('panel_time: ', meas['_id']['panel_time'])
            print('device_type: ', meas['_id']['device_type'])
            print('device_id: ', meas['_id']['device_id'])
        except KeyError as err:
            print(err)
            res = False
        finally:
            assert res is True
            out_dic.update({
                'interface_id': meas['_id']['dect_interface'],
                'device_type': 'zone' if meas['_id']['device_type'] == 0 else 'output',
                'device_id': meas['_id']['device_id']
            })

    return out_dic


def measurements_specific_device(url):
    """
    Get measurements for a specific device
    :param url: URL
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    for idx, meas in enumerate(j_con):
        if idx >= 3:
            break
        try:
            print('gas_value: ', meas['gas_value'])
            print('gas_level: ', meas['gas_level'])
            print('air_pressure: ', meas['air_pressure'])
            print('humidity: ', meas['humidity'])
            print('control_panel: ', meas['_id']['control_panel'])
            print('dect_interface: ', meas['_id']['dect_interface'])
            print('panel_time: ', meas['_id']['panel_time'])
            print('device_type: ', meas['_id']['device_type'])
            print('device_id: ', meas['_id']['device_id'])
        except KeyError as err:
            print(err)
            res = False
        finally:
            assert res is True


##############################
# #        Smart home      # #
##############################

def output_zones_get(url, header_dic):
    """
    Get list Outputs connected to the Zone
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('Output connected to {} zones'.format(len(j_cont)))
    # TBD: print real list


def zone_outputs_get(url, header_dic):
    """
    Get list Zones connected to the Output
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('Zone connected to {} outputs'.format(len(j_cont)))
    # TBD: print real list


##############################
# #       User actions     # #
##############################

def endpoint_get(url):
    """
    Get list of users
    :param url: URL
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()

    try:
        print('count:', j_cont['count'])
        for result in j_cont['results']:
            print('token:', result['token'])
            print('arn:', result['arn'])
            print('application:', result['application'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #          DECT          # #
##############################

def dect_list_get(url, header_dic, dect_ids):
    """
    Get list of paired dect devices
    :param url: URL
    :param header_dic: headers
    :param dect_ids: list
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        for device in j_cont:
            print('id:', device['id'])
            dect_ids.append(device['id'])
            print('ipud:', device['ipud'])
            print('units:')
            for unit in device['units']:
                print('\ttype:', unit['type'])
                print('\tbase_type:', unit['base_type'])
                print('\tconnected:', unit['connected'])
                print('\tid:', unit['id'])
                print('\tsub_type:', unit['sub_type'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def dect_get(url, header_dic):
    """
    Get a specific device by device_id
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.get(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('id:', j_cont['id'])
        print('ipud:', j_cont['ipud'])
        print('units:')
        for unit in j_cont['units']:
            print('\ttype:', unit['type'])
            print('\tbase_type:', unit['base_type'])
            print('\tconnected:', unit['connected'])
            print('\tid:', unit['id'])
            print('\tsub_type:', unit['sub_type'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


# def dect_measurements_latest_get():
#     print '\n[' + sys._getframe().f_code.co_name + ' on panel {}'.format(PANEL_ID) + ']'
#     url = URL_BASE + '/panels/{}/dect/measurements/latest/'.format(PANEL_ID)
#     resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
#     res = common_check(resp)
#     assert res is True
#
#     j_cont = resp.json()
#     try:
#         for meas in j_cont:
#             print 'panel_time:', meas['_id']['panel_time']
#             print 'air pressure:', meas['air_pressure']
#             print 'humidity:', meas['humidity']
#             print 'temperature:', meas['temperature'], '\n'
#     except KeyError, err:
#         print err, '\n'
#         res = False
#     finally:
#         assert res is True


