"""
IgorG
"""
import sys

import requests

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import common_check
from sub_swagger_test.url_functions.post_url_functions import TOKEN

PANEL_ID = CONFIG_DATA['panel_id']
URL_BASE = CONFIG_DATA['url_base']


def config_mode_delete(url, header_dic):
    """
    Exit configuration mode
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    # config_data = {}
    resp = requests.delete(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('config: ', j_con['config'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #           Zones        # #
##############################

def zones_zone_delete(url, header_dic):
    """
    Delete zone
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.delete(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    print('json:', j_con)
    assert j_con == {}


##############################
# #         Outputs        # #
##############################

def outputs_output_delete(url, header_dic):
    """
    Delete output
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.delete(url, headers=headers, data={})
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    print('json:', j_con)
    assert j_con == {}


##############################
# #        Troubles        # #
##############################

def troubles_delete(url, header_dic):
    """
    Reset General Troubles for a panel
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.delete(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    print('json:', j_con)
    assert j_con == {}


##############################
# #        Pictures        # #
##############################

def pictures_picture_delete(url):
    """
    Delete a specific picture by id
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN})
    print(resp)
    # res = common_check(resp)
    # assert res is True
    # print 'status code:', resp.status_code
    resp.raise_for_status()

    # j_con = resp.json()
    # print 'json:', j_con
    # assert j_con == {}


##############################
# #      Connection        # #
##############################

def connection_delete(url, header_dic):
    """
    Disconnect from the panel
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.delete(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('interface: ', j_con['interface'])
        print('connected: ', j_con['connected'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #          DECT          # #
##############################

def dect_list_delete():  # not sure it works
    print('\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID))
    url = URL_BASE + '/panels/{}/dect/'.format(PANEL_ID)
    config_data = {}

    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('jssson:', j_cont)
    try:
        print('status:', j_cont['status'])
    except KeyError as err:
        print(err)
    finally:
        assert j_cont['status'] == 'canceled'


def dect_delete(url, header_dic):
    """
    Delete/UnPair a DECT device
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    # config_data = {}

    resp = requests.delete(url, headers=headers)
    # res = common_check(resp)
    # assert res is True
    print('Status code:', resp.status_code)
    assert resp.status_code == 200

    j_cont = resp.json()
    print('json:', j_cont)
    try:
        pass
    except KeyError as err:
        print(err)
    finally:
        assert j_cont == {}


def dect_headset_delete():  # not sure it's right
    print('\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID))
    url = URL_BASE + '/panels/{}/dect/headset/'.format(PANEL_ID)
    config_data = {}

    resp = requests.delete(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    print('json:', j_cont)
    try:
        pass
    except KeyError as err:
        print(err)
    finally:
        assert j_cont == {}
