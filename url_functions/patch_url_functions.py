"""
IgorG
"""
import sys
import time

import requests
from requests.exceptions import HTTPError

from sub_swagger_test.common.common_stuff import common_check
from sub_swagger_test.common.login import TOKEN


##############################
# #           Areas        # #
##############################

def areas_area_patch(url, header_dic, state, force=False):
    """
    Change the area state
    :param url: URL
    :param header_dic: headers
    :param state: 'arm' / 'disarm'
    :param force: bool
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    assert state in ('arm', 'disarm')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    config_data = {
        "state": state,
        "force": force
    }

    resp = requests.patch(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    timeout = 70
    time_start = time.time()
    while time.time() - time_start < timeout:
        resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
        res = common_check(resp)
        assert res is True

        j_cont = resp.json()
        try:
            print('name:', j_cont['name'])
            print('id:', j_cont['id'])
            # print 'state:', j_cont['state']
            if 'in progress' in j_cont['state']:
                time.sleep(timeout/10.0)
                continue
            elif state == 'arm'and j_cont['state'] == 'armed':
                break
            elif state == 'disarm' and j_cont['state'] == 'disarmed':
                break
        except KeyError as err:
            print(err)
            res = False
        finally:
            print('state:', j_cont['state'])
            assert res is True


##############################
# #           Zones        # #
##############################

def zones_zone_patch(url, header_dic, bypass):
    print('\n[' + sys._getframe().f_code.co_name + '] bypass:{}'.format(bypass))
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    config_data = {
        "bypass": bypass
    }

    res = False
    timeout = 20
    time_start = time.time()
    while time.time() - time_start < timeout:
        # print 'inside while................'
        try:
            # resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
            # res = common_check(resp)
            # assert res is True
            resp = requests.patch(url, headers=headers, json=config_data)
            print('Status code:', resp.status_code)
            if resp.status_code == 408:
                time.sleep(timeout / 10.0)
                continue

            resp.raise_for_status()
            j_cont = resp.json()
            print('name', j_cont['name'])
            print('device_id:', j_cont['device_id'])
            print('bypass:', j_cont['bypass'])
            if bypass == j_cont['bypass']:
                print('Time elapsed:', time.time() - time_start)
                res = True
                break

        except HTTPError as err:
            print(err)
            time.sleep(timeout / 10.0)
            continue

        except KeyError as err:
            print(err)
            res = False
        # finally:
        #     print 'Inside finally...'
            # pass

    assert res is True


##############################
# #         Outputs        # #
##############################

def outputs_output_patch(url, header_dic, do_state):
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    config_data = {
        "state": do_state  # True -> switch on, False -> switch off
    }

    res = False
    timeout = 60
    time_start = time.time()
    while time.time() - time_start < timeout:
        try:
            resp = requests.patch(url, headers=headers, json=config_data)
            j_cont = resp.json()
            # print 'Response:', j_cont
            print('Message:', j_cont.get('message', None))
            print('Status code:', resp.status_code)
            if resp.status_code == 408:
                time.sleep(timeout / 10.0)
                continue

            resp.raise_for_status()
            # j_cont = resp.json()
            print('name', j_cont['name'])
            print('device_id:', j_cont['device_id'])
            print('state:', j_cont['state'])
            if do_state == j_cont['state']:
                # print 'Time elapsed:', time.time() - time_start
                res = True
                break

        except HTTPError as err:
            print(err)
            time.sleep(timeout / 10.0)
            continue

        except KeyError as err:
            print(err)
            res = False

    print('Time elapsed:', time.time() - time_start)
    assert res is True
