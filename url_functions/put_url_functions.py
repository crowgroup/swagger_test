"""
IgorG
"""
import sys
import time

import requests

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import common_check
from sub_swagger_test.common.login import TOKEN


##############################
# #         Panels         # #
##############################

def panels_panel_put(url):
    print('[' + sys._getframe().f_code.co_name + ']')

    config_data = {
        "name": "IgorG",
        "user_code": "1234",
        "control_panel": {
            "mac": "0013a1ffff23",
            "remote_access_password": "12345678",
            "longitude": 34.9097,
            "latitude": 31.98991
        }
    }

    resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('name', j_cont['name'])
        print('id:', j_cont['id'])
        print('mac:', j_cont['mac'])
        print('longitude:', j_cont['longitude'])
        print('latitude:', j_cont['latitude'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #           Areas        # #
##############################

def areas_area_put(url, header_dic):
    """
    Set an Area's configuration
    :param url: URL
    :param header_dic: headers
    """
    print('[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    config_data = {
        "name": "My Room",      # mandatory field, otherwise 400 - Bad request
        "exit_delay": 15,       # mandatory field, otherwise 400 - Bad request
        "stay_exit_delay": 25   # mandatory field, otherwise 400 - Bad request
    }

    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('name', j_cont['name'])
        print('exit_delay:', j_cont['exit_delay'])
        print('stay_exit_delay:', j_cont['stay_exit_delay'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #           Zones        # #
##############################

def zones_zone_put(url, header_dic):
    """
    Set a Zone's configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    name = 'cam2'
    config_data = {
        "name": name + " new",                                  # mandatory field, otherwise 400 - Bad request
        "device_id": CONFIG_DATA['devices'][name],  # 2325195   # mandatory field, otherwise 400 - Bad request
        "work_mode": 1,
        "stay_mode_zone": True,
        "dect_id": {
            "ipud": "12:32:f2:3d:2d",
            "unit_id": 0
        },
        "areas": [
            0
        ]
    }

    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('name', j_cont['name'])
        print('device_id:', j_cont['device_id'])
        print('stay_mode_zone:', j_cont['stay_mode_zone'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


# def zones_zone_params_put(zone_id, params=None):
#     print '[' + sys._getframe().f_code.co_name + '] id:{}'.format(zone_id)
#     url = URL_BASE + '/panels/{}/zones/{}/params/'.format(PANEL_ID, zone_id)
#
#     if params is None:
#         get_resp = requests.get(url, headers={'Authorization': 'Bearer ' + TOKEN})
#         res = common_check(get_resp)
#         assert res is True
#         params = get_resp.json()
#
#     resp = requests.put(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=params)
#     res = common_check(resp)
#     assert res is True
#
#     j_cont = resp.json()
#     for key, val in j_cont.iteritems():
#         print '{}: {}'.format(key, val)


##############################
# #         Outputs        # #
##############################

def outputs_output_put(url, header_dic):
    """
    Set an Output's configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    config_data = {
        "name": "Output new",   # mandatory field, otherwise 400 - Bad request
        "device_id": 12345321,  # mandatory field, otherwise 400 - Bad request
        "chime_mode": 0,
        "chime_timer": 0,
        "silent": False
    }

    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('name', j_cont['name'])
        print('device_id:', j_cont['device_id'])
        print('chime_mode:', j_cont['chime_mode'])
        print('chime_timer:', j_cont['chime_timer'])
        print('silent:', j_cont['silent'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


##############################
# #         Users          # #
##############################

def users_user_put(url, header_dic):
    """
    Update a specific User(by user_id)
    :param url: URL
    :param header_dic: headers
    """
    print('[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    # get_resp = requests.get(url, headers=headers)
    # res = common_check(get_resp)
    # assert res is True
    #
    # config_data = get_resp.json()
    # config_data['stay'] = not config_data['stay']

    config_data = {
        "name": "My User",
        "stay": True
    }

    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    for key, val in j_cont.items():
        print('{}: {}'.format(key, val))


##############################
# #      Connection        # #
##############################

def connection_put(url, header_dic):
    """
    Connect to the panel
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    timeout = 60
    time_start = time.time()
    while time.time() - time_start < timeout:
        resp = requests.put(url, headers=headers)
        # res = common_check(resp)
        # assert res is True
        print('Status code:', resp.status_code)
        if resp.status_code > 200:
            time.sleep(timeout / 10.0)
            continue

        j_con = resp.json()
        try:
            print('interface: ', j_con['interface'])
            print('connected: ', j_con['connected'])
            if j_con['connected']:
                res = True
                break
        except KeyError as err:
            print(err)
            res = False
        finally:
            assert res is True
    print('Time elapsed:', time.time() - time_start)


def panic_put(url, header_dic):
    """
    Make a panic alarm
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    config_data = {
        "point": {
            "longitude": 34.9097,
            "latitude": 31.98991
        }
    }
    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    assert j_con['panic_alarm']


##############################
# #    Communication       # #
##############################

def ethernet_config_put(url, header_dic):
    """
    Set Ethernet module configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    config_data = {
        "dns_server": "",
        "static_ip": "",
        "subnet_mask": "",
        "gateway": "",
        "dhcp": True,
        "enabled": True,
        "remote_control_port": 3064
    }
    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('dns_server: ', j_con['dns_server'])
        print('static_ip: ', j_con['static_ip'])
        print('subnet_mask: ', j_con['subnet_mask'])
        print('gateway: ', j_con['gateway'])
        print('dhcp: ', j_con['dhcp'])
        print('enabled: ', j_con['enabled'])
        print('remote_control_port: ', j_con['remote_control_port'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def wifi_config_put(url, header_dic):
    """
    Set WiFi module configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    config_data = {
        "enabled": True,
        "conn_config": [
            {
                "security": 0,
                "id": 0,
                "ssid": "My Wifi",
                "password": "1234567890"
            }
        ]
    }
    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('enabled: ', j_con['enabled'])
        print('id: ', j_con['id'])
        for conn in j_con['conn_config']:
            print('security: ', conn['security'])
            print('ssid: ', conn['ssid'])
            print('id: ', conn['id'])
            print('password: ', conn['password'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


def gsm_config_put(url, header_dic):
    """
    Set GSM module configuration
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    config_data = {
        "ussd": "",
        "apn": "",
        "user": "",
        "password": "",
        "pin_code": "",
        "sms": True,
        "enabled": True
    }
    resp = requests.put(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_con = resp.json()
    try:
        print('ussd: ', j_con['ussd'])
        print('apn: ', j_con['apn'])
        print('user: ', j_con['user'])
        print('password: ', j_con['password'])
        print('pin_code: ', j_con['pin_code'])
        print('sms: ', j_con['sms'])
        print('enabled: ', j_con['enabled'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True


