"""
IgorG
"""
import sys
import time

import requests
from requests.exceptions import HTTPError

from sub_swagger_test.common.common_stuff import CONFIG_DATA
from sub_swagger_test.common.common_stuff import common_check
from sub_swagger_test.common.login import TOKEN


def config_mode_post(url, header_dic):
    """
    Enter configuration mode
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    config_data = {}
    timeout = 5
    time_start = time.time()
    resp = None
    while time.time() - time_start < timeout:
        resp = requests.post(url, headers=headers, data=config_data)
        print('status code:', resp.status_code)
        try:
            resp.raise_for_status()
            break
        except HTTPError as err:
            print(err)
            time.sleep(timeout / 10.0)
            continue

    # res = common_check(resp)
    # assert res is True

    j_cont = resp.json()
    try:
        print('config: ', j_cont['config'])
    except KeyError as err:
        print(err)
        # res = False
    # finally:
    #     assert res is True


##############################
# #           Zones        # #
##############################

def zones_post(url, header_dic):
    """
    Add a new Zone for a panel
    :param url: URL
    :param header_dic: headers
    :return: zone_id
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    name = 'cam2'
    config_data = {
        "name": name,
        "device_id": CONFIG_DATA['devices'][name],  # 2325195,
        "work_mode": 0,
        "stay_mode_zone": False,
        #  "dect_id": {  Should not contain for non-DECT devices
        #    "ipud": "12:32:f2:3d:2d",
        #    "unit_id": 0
        #  },
        "areas": [
            0
        ]
    }

    resp = requests.post(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('id', j_cont['id'])
        print('device_id:', j_cont['device_id'])
        print('name:', j_cont['name'])
        print('work_mode:', j_cont['work_mode'])
        print('stay_mode_zone:', j_cont['stay_mode_zone'])
        print('type:', j_cont['type'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True

    return j_cont['id']


def zones_zone_picture_post(url, header_dic):
    """
    Take a picture in specific zone
    :param url: URL
    :param header_dic: headers
    :return: picture_id
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)
    resp = requests.post(url, headers=headers)
    print(resp)
    # res = common_check(resp)
    # assert res is True
    # print 'status code:', resp.status_code
    resp.raise_for_status()

    j_cont = resp.json()
    # print j_cont  # TODO print specific values
    res = True
    pic_id = 0
    try:
        print('control_panel:', j_cont['control_panel'])
        pic_id = j_cont['id']
        print('id:', pic_id)
        print('created:', j_cont['created'])
        print('panel_time:', j_cont['panel_time'])
        print('url:', j_cont['url'])
        print('zone:', j_cont['zone'])
        print('zone_name:', j_cont['zone_name'])
        print('event:', j_cont['event'])
        print('key:', j_cont['key'])
        print('type:', j_cont['type'])
        print('pic_index:', j_cont['pic_index'])
        print('pic_total:', j_cont['pic_total'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True

    return pic_id


##############################
# #         Outputs        # #
##############################

def outputs_post(url, header_dic):
    """
    Add a new Output for a panel
    :param url: URL
    :param header_dic: headers
    :return: output_id
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    # config_data = {
    #     "name": "Output new",
    #     "device_id": 123456321,
    #     "chime_mode": 0,
    #     "chime_timer": 0,
    #     "silent": False
    #     # "dect_id": {
    #     #     "ipud": "12:32:f2:3d:2d",
    #     #     "unit_id": 0
    #     # }
    # }
    config_data = {
        "name": "Output post",
        "device_id": 12435541,
        "chime_mode": 0,
        "chime_timer": 0,
        "silent": False
    }

    resp = requests.post(url, headers=headers, json=config_data)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    try:
        print('id', j_cont['id'])
        print('device_id:', j_cont['device_id'])
        print('name:', j_cont['name'])
        print('chime_mode:', j_cont['chime_mode'])
        print('chime_timer:', j_cont['chime_timer'])
        print('silent:', j_cont['silent'])
        print('type:', j_cont['type'])
    except KeyError as err:
        print(err)
        res = False
    finally:
        assert res is True

    return j_cont['id']


##############################
# #        Control         # #
##############################

def login(url, header_dic):
    """
    Login
    :param url: URL
    :param header_dic: headers
    """
    print('\n[' + sys._getframe().f_code.co_name + ']')
    headers = {'Authorization': 'Bearer ' + TOKEN}
    headers.update(header_dic)

    resp = requests.post(url, headers=headers)
    res = common_check(resp)
    assert res is True

    j_cont = resp.json()
    assert j_cont["status"] == "success"


##############################
# #          DECT          # #
##############################

# def dect_post():  # not sure it's right
#     print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
#     url = URL_BASE + '/panels/{}/dect/'.format(PANEL_ID)
#     config_data = {
#         "units": {
#             "type": 231,
#             "base_type": 1,
#             "connected": False,
#             "id": 1,
#             "sub_type": 263
#         },
#         "ipud": "02:7b:a0:0d:83"
#         # "id": 1
#     }
#
#     resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json={}) #config_data)
#     res = common_check(resp)
#     assert res is True
#
#     j_cont = resp.json()
#     try:
#         print 'status:', j_cont['status']
#     except KeyError, err:
#         print err
#     finally:
#         assert j_cont['status'] == 'learning'


# def dect_headset_post():  # not sure it's right
#     print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(PANEL_ID)
#     url = URL_BASE + '/panels/{}/dect/headset/'.format(PANEL_ID)
#     config_data = {}
#
#     resp = requests.post(url, headers={'Authorization': 'Bearer ' + TOKEN}, json=config_data)
#     res = common_check(resp)
#     assert res is True
#
#     j_cont = resp.json()
#     try:
#         print 'status:', j_cont['status']
#     except KeyError, err:
#         print err
#     finally:
#         assert j_cont['status'] == 'learning'

